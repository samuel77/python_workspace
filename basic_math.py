"""
A file demonstrating unit testing with doctest

Date: 11/2/2018
Author: Samuel Holmes 
"""
def addition(a, b):
    """Additon Function. Returns a + b
    >>> addition(10, 10)
    20
    """
    c = a + b
    return c

def subtraction(a, b):
    """Subtraction function. Resturns a - b
    >>> a = 30
    >>> b = 90
    >>> subtraction(a, b)
    -60
    """
    c = a - b
    return c
    
def multiplication(a, b):
    """Multiplication function. Returns a * b
    >>> a = 7
    >>> b = 9
    >>> multiplication(a, b)
    63
    """
    c = a * b
    return c

def division(a, b):
    """Division function. Returns a / b
    >>> a = 10
    >>> b = 2
    >>> division(a, b)
    5
    """

    c = a / b
    return c