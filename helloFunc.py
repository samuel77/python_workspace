#creates a Greeter object
class Greeter(object):
    #initial guest name is empty
    def __init__(self, initial_guest_name="", initial_hello="", initial_goodbye=""):
        self.guest_name = initial_guest_name
        self.string_hello = initial_hello
        self.string_goodbye = initial_goodbye
    def hello(self, guest_name):
        self.guest_name = guest_name
        string = 'Hello ' + guest_name
        self.string_hello = string
    def goodbye(self, guest_name):
        self.guest_name = guest_name
        string = 'Good-bye!' + guest_name
        self.string_goodbye = string 

