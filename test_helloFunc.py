import pytest
from helloFunc import Greeter

@pytest.fixture
def greeter():
    '''Returns a greeter with guest_name Kandy'''
    return Greeter()

@pytest.fixture
def greeter2():
    '''returns a greeter with guest_name Pam'''
    return Greeter()


def test_hello(greeter):
    greet_name = "Samuel"
    assert (greeter.guest_name != greet_name) 

def test_goodbye(greeter):
    string1 = greeter.goodbye
    string = ""
    assert (string != string1)
def test_greeter2_hello(greeter2):
    greeter2.hello("Pam")
    guest_name = "Pam"
    string1 = "Hello Pam"
    assert (string1 == greeter2.string_hello)
    assert (guest_name == greeter2.guest_name)

def test_greeter_hello(greeter):
    string1 = "Hello Bob"
    greeter.hello("Bob")
    assert (string1 == greeter.string_hello)
    