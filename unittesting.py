import unittest
import basic_math
import moves 
import address_book
import re
import address_book

class MathTests(unittest.TestCase):
    def test_five_plus_five(self):
        assert 5 + 5 == 10
    def test_one_plus_one(self):
        assert not 1 + 1 == 3
    def test_addition(self):
        x = basic_math.addition(9, 9)
        y = 18
        assert x == y 
    def test_subtraction(self):
        a = 9
        b = 10
        x = basic_math.subtraction(a, b)
        y = a - b
        assert x == y 

    def test_multiplication(self):
        a = 9
        b = 10
        x = basic_math.multiplication(a,b)
        y = a * b
        self.assertEqual(x, y)

    def test_division(self):
        a = 10
        b = 30
        x = basic_math.division(a, b)
        y = a / b
        self.assertEqual(x, y)

    def test_equal(self):
        rock1 = moves.Rock()
        rock2 = moves.Rock()
        self.assertEqual(rock1, rock2)



    def test_notEqual(self):
        rock1 = moves.Rock()
        scissor1 = moves.Scissors()
        self.assertNotEqual(rock1, scissor1)

    def test_address_book(self):
        word = 'Example'
        assert address_book.readFromFile(word)


        
    @unittest.expectedFailure
    def test_address_book2(self):
        word = 'asdfasdfasdf'
        assert address_book.readFromFile(word)
    


if __name__ == '__main__':
    unittest.main()