import re

def readFromFile(word): 
    names_file = open("names.txt", encoding="utf-8")
    data = names_file.read()
    names_file.close()
    #print(data)

    #match at beginning of string
    #print(re.match(r'Love', data))

    #match in the string somewhere
    #print(re.search(r'Kenneth', data))
    print(re.findall(r'\(?\d{3}\)?-?\s?\d{3}-\d{4}',data))
    print(re.findall(r'\w*, \w+', data))
    print(re.findall(r'\w{4,}', data))
    print(re.findall(r'[-\w\d+.]+@[-\w\d.]+', data))
    random_word = word
    if (re.search(random_word, data)) != None:
        return True
    else:
        return False
print(readFromFile("Example"))


